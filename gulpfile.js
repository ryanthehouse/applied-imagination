var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');

// Static server
gulp.task('watch', function() {
    browserSync.init({
	    proxy: "http://localhost/applied%20imagination/",
	    port: 8000  
	 });

	 gulp.watch('./resources/scss/**/*.scss', ['scss']);
     gulp.watch('./resources/views/**/*.php', ['php']);
     gulp.watch('./public/**/*.php', ['php']);
     gulp.watch('./public/**/*.js', ['js']);
});

gulp.task('scss', function(){
	return gulp.src('./resources/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./public/css'))
        .pipe(browserSync.reload({
        	stream: true
        }));
});

gulp.task('php', function(){
    browserSync.reload();
});

gulp.task('js', function(){
    browserSync.reload();
});