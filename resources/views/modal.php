<div class="modalWrap">
	<div class="modal">
		<div class="modalClose">
			<i class="fa fa-times-circle"></i>
		</div>
		<div class="modalHeader">
			<h1 class="chalk title">Assembly Guide</h1>
			<p>It takes about a minute to assemble</p>
		</div>
		<div class="modalBody">
			<div class="block">
				<img src="./images/fileDownload.jpg">
				<a href="#" class="blockLink">Download PDF</a>
			</div>
			<div class="block">
				<img src="./images/video.jpg">
				<a href="#" class="blockLink">Watch the Video</a>
			</div>
		</div>
	</div>
</div>