<div class="header">
	<div class="kickStartCTA">
		Back us on <div class="imageWrap"><img src="./images/kickStarterLogo.jpg"></div>
	</div>
	<nav class="headerNav">
		<li class="navItem"><a href="#" class="assemblyModalControl">Assembly Guide</a></li>
		<li class="navItem"><a href="#">Contact Us</a></li>
		<li class="navItem navButton"><a href="#">Buy Now</a></li>
	</nav>
</div>