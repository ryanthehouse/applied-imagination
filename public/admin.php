<?php 
	// Include config file
	require_once '../database/config.php';
	include '../resources/auth/auth.php';

	// Get all comments
	$sql = 'SELECT * FROM comments';
	$comments = mysqli_query($link, $sql);

	$pendingComments = [];

	$approvedComments = [];

	// Sort comments by approved status
	foreach ($comments as $comment) {
		// print_r($comment);
		if ($comment['approved']) {
			$approvedComments[] = $comment;
		}
		else{
			$pendingComments[] = $comment;
		}
	}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
  	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>
		Index
	</title>
	<link rel="stylesheet" type="text/css" href="css/all.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
</head>
<body>
	<div class="page">
		<!-- Header -->
		<?php 
			include '../resources/views/adminHeader.php';
		?>

		<!-- Comment Management -->
		<div class="commentManagement">
			<div class="commentLists">
				<h1>Comments</h1>
				<!-- Pending Comments -->
				<h2 class="listTitle">Awaiting Approval</h2>
				<ul class="commentList commentsPending">
					<?php 
						if(count($pendingComments) > 0){
							foreach ($pendingComments as $comment) {
							?>
								<li class="commentListing">
									<div class="details">
										<p class="message"><?php echo $comment['message'] ?></p>
										<small class="author"><?php echo $comment['author'] ?></small>
										<small class="date"><?php
											$date = new DateTime($comment['created_date']);
											echo $date->format('n/j/Y');
										?></small>
									</div>
									<div class="actions">
										<a class="action" href="./comments/edit?id=<?php echo $comment['id'] ?>"><i class="fa fa-edit"></i></a>
										<a class="action" href="./comments/delete?id=<?php echo $comment['id'] ?>" onclick="return confirm('Are you sure, you want to delete this comment?')"><i class="fa fa-trash"></i></a>
										<a class="action" href="./comments/approve?id=<?php echo $comment['id'] ?>"><i class="fa fa-check-circle"></i></a>
										<a class="action" href="./comments/delete?id=<?php echo $comment['id'] ?>" onclick="return confirm('Are you sure, you want to deny this comment?')"><i class="fa fa-times-circle"></i></a>
									</div>
								</li>
							<?php
							}
						}
						else{
						?>
							<li class="commentListing">No Pending Comments</li>			
						<?php
						}
						
					?>				
				</ul>
				<!-- Approved Comments -->
				<h2 class="listTitle">Approved</h2>
				<ul class="commentList">
					<?php 
						if(count($approvedComments) > 0){
							foreach ($approvedComments as $comment) {
							?>
								<li class="commentListing">
									<div class="details">
										<p class="message"><?php echo $comment['message'] ?></p>
										<small class="author"><?php echo $comment['author'] ?></small>
										<small class="date"><?php
											$date = new DateTime($comment['created_date']);
											echo $date->format('n/j/Y');
										?></small>
									</div>
									<div class="actions">
										<a class="action" href="./comments/edit?id=<?php echo $comment['id'] ?>"><i class="fa fa-edit"></i></a>
										<a class="action" href="./comments/delete?id=<?php echo $comment['id'] ?>" onclick="return confirm('Are you sure, you want to delete this comment?')"><i class="fa fa-trash"></i></a>
									</div>
								</li>
							<?php
							}
						}
						else{
						?>
							<li class="commentListing">
								No Approved Comments
							</li>
						<?php
						}
					?>				
				</ul>
			</div>
		</div>
	</div>
</body>
</html>