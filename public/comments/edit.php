<?php
	include '../../resources/auth/auth.php';
	include '../../database/connection.php';

	// Get comment to edit
	$sql = 'SELECT * FROM comments WHERE id = '.$_GET["id"];
	$results = mysqli_query($link, $sql);
	$comment = $results->fetch_assoc();

	// Close connection
    mysqli_close($link);

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
  	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>
		Edit Comment
	</title>
	<link rel="stylesheet" type="text/css" href="../css/all.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>		
	<div class="page">
		<!-- Header -->
		<?php 
			include '../../resources/views/adminHeader.php';
		?>
		<div class="formWrap">
			<div class="form">
				<h1 class="title">Edit Comment</h1>
				<form action="./save" method="post">
					<input type="hidden" name="id" value="<?php echo $comment['id'] ?>">
					<div class="inlineField">
						<label for="message">Comment</label>
						<textarea cols="10" rows="5" name="message" id="message"><?php echo $comment['message'] ?></textarea>
					</div>
					<div class="inlineField">
						<label for="author">By</label>
						<input type="text" name="author" id="author" value="<?php echo $comment['author'] ?>">
					</div>
					<div class="inlineField">
						<label for="created_date">Date</label>
						<input type="date" name="created_date" id="created_date" value="<?php 
							$date = new DateTime($comment['created_date']);
							echo $date->format('Y-m-d');
						?>">
					</div>
					<input type="submit" name="submit" value="Go" class="formAction">
					<input type="button" name="cancel" value="Cancel" class="formAction" onclick="javascript:window.location='../admin';">
				</form>
			</div>
		</div>
	</div>
</body>
</html>