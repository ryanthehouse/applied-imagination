window.jQuery = window.$ = jQuery;

$(document).ready(function(){

	// Add modal toggle control
	$('.assemblyModalControl').click(function(){
		$('.modalWrap').addClass('show');
	});

	$('.modalClose').click(function(){
		$('.modalWrap').removeClass('show');
	});
});