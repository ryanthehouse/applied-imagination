<?php 
	// Include config file
	require_once '../database/config.php';

	// Get all approved comments
	$sql = 'SELECT * FROM comments WHERE approved = 1';
	$comments = mysqli_query($link, $sql);
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
  	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>
		Index
	</title>
	<link rel="stylesheet" type="text/css" href="css/all.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
	<script type="text/javascript" src="js/all.js"></script>
</head>
<body>
	<div class="page">
		<!-- Modal -->
		<?php include '../resources/views/modal.php' ?> 

		<!-- Header -->
		<?php include '../resources/views/header.php' ?>
		
		<!-- Above the fold -->
		<div class="contentSeg gradientSeg">
			<div class="splashContent">
				<div class="segHalf centered">
					<img class="logo" src="./images/logo.svg"></img>
					<img class="heroImage" src="./images/diceTower.png">
				</div>
				<div class="segHalf">
					<h1 class="title chalk underlined">Get 3 Dice Towers for $25!</h1>
					<p>Anyone who plays board games knows that rolling dice is a pain in the ass. You lose them under furniture. You ruin your board games by knocking over all your pieces.</p>
					<p>A common solution is to get a dice tower but dice towers aren't cheap. They can range from $30 to $200. But... if all its meant to do is roll dice, why is it so expensive?</p>
					<div class="ctaGroup">
						<a class="button">Buy Now</a>
						<a class="button">Watch Videos</a>
					</div>
				</div>
			</div>	
			<div class="overviewCardsWrap">
				<ul class="overviewCards">
					<?php 
						$cardItems = array(
							'Recycled Cardboard' => 'recycle',
							'1 minute Assembly' => 'timer',
							'Store Easily' => 'tractor',
							'Practical' => 'die',
							'Durable' => 'shield'
						);

						foreach($cardItems as $title => $img){ ?>
							<li class="overviewCard">
								<img src="./images/<?php echo $img ?>.png">
								<p class="title"><?php echo $title ?></p>
							</li>
						<?php }
					?>
				</ul>
			</div>
		</div>

		<!-- Watch the Video -->
		<div class="contentSeg centeredSeg corkedSeg">
			<h1 class="chalk title">Watch the Video</h1>
			<div class="flareFrame">
				<img src="./images/stars1.png">
			</div>
			<div class="flareContent">
				<div class="videoWrap">
					<video width="100%" height="100%" src="./videos/towerReview.mp4" frameborder="0" class="videoDemo block" controls></video>
				</div>
			</div>
			<div class="flareFrame">
				<img src="./images/stars2.png">
			</div>
		</div>

		<!-- Happy Family -->
		<div class="contentSeg centeredSeg splitOverlap">
			<h1 class="chalk title">3 Dice Towers<br> means you don't need to pass it around</h1>
			<div class="shardImage">
				<img class="shard" src="./images/shards1.png">
				<img class="block" src="./images/family.jpg">
				<img class="shard" src="./images/shards2.png">
			</div>
		</div>

		<!-- Pricing -->
		<div class="contentSeg centeredSeg gradientSeg">
			<h1 class="chalk title underlined contained">Pricing</h1>
			<ul class="pricingOptions block">
				<?php 
					$priceOptions = array(
						'1' => [
							'value' => '3x',
							'title' => '3 Dice Tower Kit',
							'price' => '25'
						],
						'2' => [
							'value' => '10x',
							'title' => '10 Dice Tower Kit',
							'price' => '70'
						],
						'3' => [
							'value' => '100x',
							'title' => '100 Dice Tower Kit',
							'price' => '400'
						]
					);
					foreach($priceOptions as $option){
					?>
						<li class="priceOption">
							<h1 class="value chalk"><?php echo $option['value']  ?></h1>
							<p class="title"><?php echo $option['title']  ?></p>
							<h2 class="price"><span class="mini">$</span><?php echo $option['price'] ?></h2>
							<a class="cta" href="#">Purchase Kit</a>
						</li>
					<?php
					}
				?>
			</ul>
		</div>

		<!-- How to assemble -->
		<div class="contentSeg centeredSeg corkedSeg">

			<h1 class="chalk title">How to Assemble</h1>
			<div class="flareFrame">
				<img src="./images/stars3.png">
			</div>
			<div class="flareContent">
				<div class="videoWrap">
					<video width="100%" height="100%" src="./videos/towerReview.mp4" frameborder="0" class="videoDemo block" controls></video>
				</div>
			</div>
			<div class="flareFrame">
				<img src="./images/stars4.png">
			</div>
		</div>

		<!-- Kick Starter banner -->
		<div class="contentSeg centeredSeg">
			<div class="kickStartBanner">
				Back us on <img src="./images/kickStarterLogo.jpg">
			</div>
		</div>

		<!-- Leave a comment -->
		<div class="contentSeg centeredSeg formSeg">
			<h1 class="chalk title underlined contained">Leave your Comment</h1>
			<div class="comment">
				<form action="./comments/new" method="post">
					<div class="formRow">
						<div class="formGroup">
							<label for="author">Your Name</label>
							<input type="text" name="author" id="author">
						</div>

						<div class="formGroup">
							<label for="author_email">Email Address</label>
							<input type="text" name="author_email" id="author_email">
						</div>
					</div>


					<div class="formRow">
						<div class="formGroup">
							<label for="message">Comment</label>
							<textarea cols="10" rows="5" name="message" id="message"></textarea>
						</div>
					</div>

					</textarea>			
			
					<div class="formRow">
						<input type="submit" name="submit" value="Comment" class="formAction">
					</div>
				</form>
			</div>
		</div>

		<!-- Comments -->
		<div class="contentSeg centeredSeg">			
			<ul class="commentList">
				<h1 class="chalk title">Comments</h1>
				<?php
					foreach ($comments as $comment) {
					?>
						<li class="commentListing">
							<div class="details">
								<p class="message"><?php echo $comment['message'] ?></p>
								<small class="author"><?php echo $comment['author'] ?></small>
								<small class="date"><?php
									$date = new DateTime($comment['created_date']);
									echo $date->format('n/j/Y');
								?></small>
							</div>
						</li>
					<?php
					}
				?>
			</ul>
		</div>
	</div>
</body>
</html>